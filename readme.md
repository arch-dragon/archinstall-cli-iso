
## About
This project builds a custom arch iso with [archinstall-cli](https://gitlab.com/arch-dragon/archinstall-cli) and its requirements.

## How to use:

1. Make sure you have git, and archiso installed
1. Clone this repository
1. cd into the repository's folder
1. Run ./build.sh
1. The resulting iso will be insde .out/
