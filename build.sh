#!/bin/bash

command_exists () {
    command -v $1 >/dev/null 2>&1;
}

# check requirements

if ! command_exists mkarchiso; then
    echo "ERROR: archiso is not installed"
    exit 1
fi

if ! command_exists git; then
    echo "ERROR: git is not installed"
    exit 1
fi

# cleanup

if [ -d "archlive" ]; then
    rm -r archlive
fi
if [ -d "archinstall-cli" ]; then
    rm -r archinstall-cli
fi
if [ -d "out" ]; then
    sudo rm -r out
fi
if [ -d "work" ]; then
    sudo rm -r work
fi

# preparations

lastArchisoTag=$(git describe --tags --abbrev=0)

cp -r /usr/share/archiso/configs/releng/ archlive

git clone https://gitlab.com/arch-dragon/archinstall-cli.git
if [ ! -d "archinstall-cli" ]; then
    echo "ERROR: archinstall-cli is not present"
    exit 2
fi
cd archinstall-cli
lastArchinstallTag=$(git describe --tags --abbrev=0)
git checkout ${lastArchinstallTag}
rm -rf .git
cd ..

mv archinstall-cli archlive/airootfs/root/

cp -r modifications/* archlive/

sed -i 's/$(date +%Y.%m.%d)/v'"${lastArchinstallTag}+v${lastArchisoTag}-$(date +%Y.%m.%d)"'/' archlive/profiledef.sh

# ---------

# add additional packages

echo "git" | tee -a archlive/packages.x86_64

# create iso
sudo mkarchiso -v -w ./work -o ./out archlive
sudo chmod 777 -R ./out

exit 0